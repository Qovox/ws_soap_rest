﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace RESTGui
{
    public partial class JCDecauxApplication : Form
    {
        /// <summary>
        /// 0 = SearchButton
        /// 1 = ContractsButton
        /// 2 = StationsButton
        /// </summary>
        private Int16 focus = 0;

        private Boolean cityTextBoxReady = false;
        private Boolean stationTextBoxReady = false;

        private JCDecauxRequestHandler requestHandler = new JCDecauxRequestHandler();

        public JCDecauxApplication()
        {
            InitializeComponent();
            SearchValidateButton.Enabled = false;
            ButtonSidePanel.Height = SearchButton.Height;
            ButtonSidePanel.Top = SearchButton.Top;
        }

        // Search button

        private void Search_Click(object sender, EventArgs e)
        {
            ButtonSidePanel.Height = SearchButton.Height;
            ButtonSidePanel.Top = SearchButton.Top;
            CityTextBox.Enabled = true;
            StationTextBox.Enabled = true;
            focus = 0;
            CheckSearchActivateButtonState();
        }

        // Contracts button

        private void Contracts_Click(object sender, EventArgs e)
        {
            ButtonSidePanel.Height = ContractsButton.Height;
            ButtonSidePanel.Top = ContractsButton.Top;
            CityTextBox.Enabled = false;
            StationTextBox.Enabled = false;
            focus = 1;
            CheckSearchActivateButtonState();
        }

        // Stations button

        private void Stations_Click(object sender, EventArgs e)
        {
            ButtonSidePanel.Height = StationsButton.Height;
            ButtonSidePanel.Top = StationsButton.Top;
            CityTextBox.Enabled = true;
            StationTextBox.Enabled = false;
            focus = 2;
            CheckSearchActivateButtonState();
        }

        // City text box

        private void CityTextBox_TextChanged(object sender, EventArgs e)
        {
            if (CityTextBox.Text.Length > 0)
            {
                cityTextBoxReady = true;
            }
            else
            {
                cityTextBoxReady = false;
            }
            CheckSearchActivateButtonState();
        }

        // Station text box

        private void StationTextBox_TextChanged(object sender, EventArgs e)
        {
            if (StationTextBox.Text.Length > 0)
            {
                stationTextBoxReady = true;
            }
            else
            {
                stationTextBoxReady = false;
            }
            CheckSearchActivateButtonState();
        }

        // Search validate button

        private void SearchValidateButton_Click(object sender, EventArgs e)
        {
            switch (focus)
            {
                case 0:
                    Results.Text = requestHandler.RequestSearch(CityTextBox.Text, StationTextBox.Text).Replace("\n", Environment.NewLine);
                    break;
                case 1:
                    Results.Text = requestHandler.RequestContracts().Replace("\n", Environment.NewLine); ;
                    break;
                case 2:
                    Results.Text = requestHandler.RequestStations(CityTextBox.Text).Replace("\n", Environment.NewLine); ;
                    break;
            }
        }

        // JCDecaux Paint

        private void JCDecaux_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://www.jcdecaux.fr/");
            Process.Start(sInfo);
        }

        private void CheckSearchActivateButtonState()
        {
            switch (focus)
            {
                case 0:
                    if (cityTextBoxReady && stationTextBoxReady)
                    {
                        SearchValidateButton.Enabled = true;
                    }
                    else
                    {
                        SearchValidateButton.Enabled = false;
                    }
                    break;
                case 1:
                    SearchValidateButton.Enabled = true;
                    break;
                case 2:
                    if (cityTextBoxReady)
                    {
                        SearchValidateButton.Enabled = true;
                    }
                    else
                    {
                        SearchValidateButton.Enabled = false;
                    }
                    break;
            }
        }

    }
}
